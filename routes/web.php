<?php

use Illuminate\Support\Facades\Route;
use Http\Controllers\CantanteController;
use Http\Controllers\CanzoneController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/cantanti', [CantanteController::class, 'index']);
Route::post('/cantanti', [CantanteController::class, 'store']);
Route::get('/canzoni', [CanzoneController::class, 'index']);
Route::post('/canzoni', [CanzoneController::class, 'store']);
Route::get('/canzoni', [CanzoneController::class, 'canzoniByCantante']);
