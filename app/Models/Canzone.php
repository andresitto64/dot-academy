<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Canzone extends Model
{
    use HasFactory;

    public function cantanti()
{
    return $this->belongsToMany(Cantante::class);
}

}
