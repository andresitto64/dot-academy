<h1>Canzoni di {{ $cantante->nome }}</h1>

<a href="/cantanti">Torna ai Cantanti</a>

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Titolo</th>
      <th>Data di Pubblicazione</th>
      <th>Cantanti</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($canzoni as $canzone)
      <tr>
