<h1>Aggiungi Canzone</h1>

<form method="post" action="/canzoni">
  @csrf
  <label for="titolo">Titolo:</label>
  <input type="text" name="titolo"><br>

  <label for="data_pubblicazione">Data di Pubblicazione:</label>
  <input type="date" name="data_pubblicazione"><br>

  <label for="cantanti[]">Cantanti:</label>
  <select name="cantanti[]" multiple>
    @foreach($cantanti as $cantante)
      <option value="{{ $cantante->id }}">{{ $cantante->nome }}</option>
    @endforeach
  </select><br>

  <input type="submit" value="Aggiungi">
</form>
