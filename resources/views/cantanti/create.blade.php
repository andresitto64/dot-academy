<h1>Aggiungi Cantante</h1>

<form method="post" action="/cantanti">
  @csrf
  <label for="nome">Nome:</label>
  <input type="text" name="nome"><br>

  <label for="data_nascita">Data di Nascita:</label>
  <input type="date" name="data_nascita"><br>

  <label for="sesso">Sesso:</label>
  <input type="radio" name="sesso" value="M"> Maschio
  <input type="radio" name="sesso" value="F"> Femmina<br>

  <input type="submit" value="Aggiungi">
</form>
