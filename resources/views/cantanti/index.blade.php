<h1>Tutti i Cantanti</h1>

<a href="/cantanti/create">Aggiungi Cantante</a>

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Nome</th>
      <th>Data di Nascita</th>
      <th>Sesso</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($cantanti as $cantante)
      <tr>
        <td>{{ $cantante->id }}</td>
        <td>{{ $cantante->nome }}</td>
        <td>{{ $cantante->data_nascita }}</td>
        <td>{{ $cantante->sesso }}</td>
        <td><a href="/canzoni/cantante/{{ $cantante->id }}">Visualizza Canzoni</a></td>
      </tr>
    @endforeach
  </tbody>
</table>
